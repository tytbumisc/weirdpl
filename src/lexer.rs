pub enum TokenType{
    directive,
    delim,
    id,
    keyword,
    comment,
    lit
}


enum State{
    Nothing,
}


struct Lexer{
    symbols: Vec<(TokenType, String)>,
    state: State,
    input: &String
}

impl Lexer{
    pub fn new(inp: &String){
        return Lexer{symbols: Vec::new(), state: State::Nothing, input: inp}
    }
    pub fn next(ch: char){
        
    }
}
