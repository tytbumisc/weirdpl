mod data_types{
    trait DataType {
        pub fn getType() -> string;
        pub fn value();
    }   

    trait Int: DataType{
        pub fn value(&self) -> i64;
    }
    struct Integer {
        value: i64
    }
    impl Int for Integer{
        fn value(&self) {self.value}
        fn getType(){"int"}
    }

    trait Float: DataType{
        pub fn value() -> f64;
    }
    struct FloatingPoint {
        value: f64
    }
    impl Float for FloatingPoint{
        fn value(&self) {self.value}
        fn getType() {"float"}
    }    

    trait Bool: DataType{
        pub fn value(&self) -> bool;
    }
    struct Boolean {
        value: bool
    }
    impl Bool for Boolean{
        fn value(&self) {self.value}
        fn getType() {"bool"}
    }


    struct Function{
        args: Vec<impl DataType>,
        out: impl DataType,
        function: &string
    }
}
